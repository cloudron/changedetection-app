#!/bin/bash

set -eu

mkdir -p /app/data/datastore /run/changedetection/workspace

readonly config_file=/app/data/datastore/url-watches.json

# this turns off version check
export GITHUB_REF=${CD_VERSION}
export PYTHONPATH=/app/code/changedetection.io/dependencies
export PYTHONUNBUFFERED=1

# https://github.com/browserless/chrome/blob/master/Dockerfile
export NODE_ENV=production
#export WORKSPACE_DIR=/run/changedetection/workspace

export CHROME_BIN="$(python3 -c 'from playwright.sync_api import sync_playwright; print(sync_playwright().start().chromium.executable_path)')"
export CHROME_PATH=$(dirname "${CHROME_BIN}")

cat > /run/changedetection/env <<EOT
SCREEN_WIDTH=1920
SCREEN_HEIGHT=1024
SCREEN_DEPTH=16
ENABLE_DEBUGGER=false
CONCURRENT=10
IS_DOCKER=true
LANG="C.UTF-8"
CHROME_BIN=${CHROME_BIN}
CHROME_PATH=${CHROME_PATH}
EOT

if [[ ! -f "${config_file}" ]]; then
    echo "=> init on first run"
    /usr/bin/python3.10 ./changedetection.py -d /app/data/datastore -p 5000 &
    cd_pid=$!
    sleep 5
    cpids=`pgrep -P ${cd_pid}|xargs`
    for cpid in $cpids;
    do
        kill -SIGHUP ${cpid}
    done
    kill -SIGTERM ${cd_pid}

    cat $config_file |
        jq ".settings.application.password = \"1WfOi8p10pZ/yswPC4Xde1j0ETBt857Aou4Ay6A546RXxwPJHGZz75b9xKJp8H4OVS7RePKVRAjqeFoaDMlX7g==\"" | \
        sponge $config_file
fi

echo "=> updating config file"
cat $config_file |
    jq ".settings.application.base_url = \"${CLOUDRON_APP_ORIGIN}\"" | \
    sponge $config_file

echo "=> changing ownership"
chown -R cloudron:cloudron /app/data

echo "=> starting changedetection.io"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i changedetection.io
