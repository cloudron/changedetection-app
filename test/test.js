#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;
    const password = 'changeme';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(password) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.xpath('//input[@type="password"]'));
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.sleep(5000);
        await waitForElement(By.xpath('//legend[contains(text(), "Add a new change detection watch")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.manage().deleteAllCookies();
        await browser.sleep(2000);
    }

    async function getMainPage() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//input[@id="watch_submit_button"]'));
    }

    async function changeFetchMethod() {
        await browser.get(`https://${app.fqdn}/settings#fetching`);
        await browser.findElement(By.xpath('//label[contains(text(), "Playwright Chromium/Javascript")]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@id="save_button"]')).click();
        await browser.sleep(2000);
    }

    async function createChangeDetection(url, tag) {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//input[@name="url"]'));
        await browser.findElement(By.xpath('//input[@name="url"]')).sendKeys(url);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[contains(@placeholder, "watch label")]')).sendKeys(tag);
        await browser.findElement(By.xpath('//input[@id="watch_submit_button"]')).click();
        await waitForElement(By.xpath(`//a[contains(text(), "${tag}")]`));
        console.log('Waiting for 15 seconds for the page to be fetched');
        await browser.sleep(15000);
    }

    async function checkChangeDetection(tag) {
        await browser.get(`https://${app.fqdn}/?tag=${tag}`);

        const previewLink = await browser.findElement(By.xpath('//a[contains(text(), "Preview")]')).getAttribute('href');
        console.log(`The preview link is ${previewLink}`);

        await browser.get(previewLink);
        await waitForElement(By.xpath('//*[contains(text(), "Self-hosting")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, password));
    it('can get the main page', getMainPage);

    it('can change fetch method', changeFetchMethod);
    it('can add new Change Detection', createChangeDetection.bind(null, 'https://blog.cloudron.io', 'Cloudron'));
    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(20000);
    });

    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await browser.sleep(20000);
    });

    it('can get app information', getAppInfo);

    xit('can login', login.bind(null, password));

    it('can get the main page', getMainPage);
    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, password));
    it('can get the main page', getMainPage);
    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id io.changedetection.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        // wait when all services are up and running
        await browser.sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, password));
    it('can get the main page', getMainPage);
    it('can change fetch method', changeFetchMethod);
    it('can add new change detection', createChangeDetection.bind(null, 'https://blog.cloudron.io', 'Cloudron'));
    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('check Change Detection', checkChangeDetection.bind(null, 'Cloudron'));

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
