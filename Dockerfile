FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/changedetection.io /app/pkg /app/code/browserless /app/code/browsers /app/code/sockpuppetbrowser

WORKDIR /app/code/changedetection.io

ENV NODE_ENV=production
ENV PUPPETEER_SKIP_DOWNLOAD=true
ENV PLAYWRIGHT_BROWSERS_PATH=/app/code/browsers

# the 0 prefix in patch release is ignored since it is not valid semver
# renovate: datasource=github-releases depName=dgtlmoon/changedetection.io versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.0*(?<patch>\d+)$
ENV CD_VERSION=0.49.4

RUN curl -sL https://github.com/dgtlmoon/changedetection.io/archive/refs/tags/${CD_VERSION}.tar.gz | tar --strip-components 1 -xvzf -

RUN pip3 install --target=/app/code/changedetection.io/dependencies -r requirements.txt && \
    pip3 install --target=/app/code/changedetection.io/dependencies playwright~=1.42.0

# https://github.com/dgtlmoon/sockpuppetbrowser
WORKDIR /app/code/sockpuppetbrowser
RUN curl -sL https://github.com/dgtlmoon/sockpuppetbrowser/archive/4e2ff39ba367f0114e00b5f0c86720ee798de143.tar.gz | tar --strip-components 1 -xvzf -

RUN apt-get update && \
    apt-get -y -qq install python3.10-venv
RUN pip3 install --no-cache --upgrade setuptools virtualenv
RUN python3 -m venv . && source ./bin/activate && ./bin/pip3 install -r requirements.txt


# Install chrome stable (https://github.com/browserless/browserless/tags)
ARG BROWSERLESS_VERSION=2.4.0

RUN apt-get update && \
    apt-get install  -y --no-install-recommends fonts-liberation libatk-bridge2.0-0 libatk1.0-0 libatspi2.0-0 libgtk-3-0 \
        libnspr4 libnss3 libu2f-udev libxcomposite1 libxdamage1 xdg-utils xvfb libvulkan1

WORKDIR /app/code/browserless

RUN echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections && \
  apt-get -y -qq install software-properties-common &&\
  apt-add-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -sc) partner" && \
  apt-get -y -qq --no-install-recommends install \
    fontconfig \
    fonts-freefont-ttf \
    fonts-gfs-neohellenic \
    fonts-indic \
    fonts-ipafont-gothic \
    fonts-kacst \
    fonts-liberation \
    fonts-noto-cjk \
    fonts-noto-color-emoji \
    fonts-roboto \
    fonts-thai-tlwg \
    fonts-ubuntu \
    fonts-wqy-zenhei

RUN curl -sL https://github.com/browserless/browserless/archive/refs/tags/v${BROWSERLESS_VERSION}.tar.gz | tar --strip-components 1 -xvzf - && \
    npm i

RUN ./node_modules/.bin/playwright-core install --with-deps chromium && \
  npm run build && \
  npm run build:function && \
  npm prune production && \
  fc-cache -f -v && \
  apt-get -qq clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/fonts/truetype/noto


# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/changedetection/supervisord.log /var/log/supervisor/supervisord.log

WORKDIR /app/code/changedetection.io

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
